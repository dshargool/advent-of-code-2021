from collections import Counter
import statistics

def fuel_cost_step(steps):
    fuel = 0
    for i in range(1,steps+1):
        fuel = fuel + i
    return fuel

def get_fuel_cost(positions, end_position):
    fuel = 0
    positions = Counter(positions)
    for position in positions:
        fuel += fuel_cost_step(abs(position - end_position)) * positions[position]
    return fuel

def optimal_position(positions):
    old_cost = float('inf')
    for position in range(max(positions)):
        cost = get_fuel_cost(positions,position)
        if old_cost > cost:
            old_cost = cost
            best_position = position
    return best_position

    
file = open('./dec_7/input.txt','r')
crab_positions = file.read().split(',')
crab_positions = list(map(int, crab_positions))
opt_position = optimal_position(crab_positions)
fuel_cost = get_fuel_cost(crab_positions, opt_position)
print(fuel_cost)