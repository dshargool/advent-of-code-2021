def draw_line(x1, y1, x2, y2, map):
    # Left to right line
    if x1 <= x2:
        init_x = x1
        end_x = x2
    # Right to left line
    elif x1 > x2:
        init_x = x2
        end_x = x1
    # Top to bottom line
    if y1 <= y2:
        init_y = y1
        end_y = y2
    # Bottom to top line
    elif y1 > y2:
        init_y = y2
        end_y = y1

    for i in range(init_x, end_x+1):
        for j in range(init_y, end_y+1):
            map[j][i] = map[j][i] + 1

def calc_danger_zones(map):
    score = 0
    for rows in map:
        for cols in rows:
            if cols > 1:
                score += 1
    return score

map_size = 1000
map = [[0]*map_size for i in range(map_size)]
file = open('input.txt', 'r')
for line in file:
    coords = line.split()
    orig = coords[0].split(',')
    end = coords[2].split(',')
    x_1 = int(orig[0])
    y_1 = int(orig[1])
    x_2 = int(end[0])
    y_2 = int(end[1])

    if x_1 == x_2 or y_1 == y_2:
        draw_line(x_1, y_1, x_2, y_2, map)

print(calc_danger_zones(map))