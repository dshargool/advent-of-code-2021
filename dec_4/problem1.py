import copy

def board_win(board):
    row_win = True
    col_win = True
    # Check the rows if there's a win
    for i in range(5):
        for j in range(5):
            #Check rows for a winner
            if board[i][j] == True:
                row_win = board[i][j] & row_win
            else:
                row_win = False
            # Check columns at the same time
            if board[j][i] == True:
                col_win = board[j][i] & col_win
            else:
                col_win = False
        if row_win or col_win:
            return True

def calc_board_score(board):
    score = 0
    for i in range(5):
        for j in range(5):
            if board[i][j] != True:
                score = score + int(board[i][j])
    return score


file = open('input.txt', 'r')
bingo_called_num = file.readline()
bingo_called_num = bingo_called_num.split(",")
lowest_steps = len(bingo_called_num)
board = [[]] * 5
for lines in file:
    # It's a blank line so we know there's a new board
    if lines == "\n":
        new_board = True
        for i in range(0, 5):
            board[i] = file.readline().split()
        steps_to_win = 0
        for nums in bingo_called_num:
            # Apply the pulled number to our board
            for row, row_vals in enumerate(board):
                for col, col_val in enumerate(row_vals):
                    if nums == col_val:
                        board[row][col] = True
            # Check the board to see if it wins, if it doesn't we add a step.
            if not board_win(board):
                steps_to_win += 1
            else:
                # Save the current winning board
                if lowest_steps > steps_to_win:
                    lowest_steps = steps_to_win
                    winning_board = copy.deepcopy(board)
                    winning_num_called = int(nums)
                break
print(calc_board_score(winning_board) * winning_num_called)