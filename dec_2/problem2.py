file = open('inputs.txt', 'r')
horizontal = 0
depth = 0
aim = 0
for lines in file:
    command = lines.split()
    direction = command[0]
    amount = int(command[1])
    if direction == "forward":
        horizontal = horizontal + amount
        depth = depth + amount * aim
    elif direction == "up":
        aim = aim - amount
    elif direction == "down":
        aim = aim + amount
print(horizontal * depth)
