# Advent of Code 2021
All of my code for Advent of Code 2021.  Goal is to complete them daily and get all the stars.

## What is it?
[Advent of Code About](https://adventofcode.com/2021/about)

Advent of Code is an Advent calendar of small programming puzzles for a variety of skill sets and skill levels that can be solved in any programming language you like. People use them as a speed contest, interview prep, company training, university coursework, practice problems, or to challenge each other.

You don't need a computer science background to participate - just a little programming knowledge and some problem solving skills will get you pretty far. Nor do you need a fancy computer; every problem has a solution that completes in at most 15 seconds on ten-year-old hardware.

If you'd like to support Advent of Code, you can do so indirectly by helping to [Shareon Twitter Mastodon] it with others, or directly via PayPal or Coinbase.

Advent of Code is a registered trademark in the United States.
