from collections import Counter
import copy


def get_oxy_common_bit(column):
    # For the oxygen we want the more common number or 1 if equal amounts
    most = Counter(column).most_common(2)
    if len(most) > 1 and most[0][1] == most[1][1]:
        # If there are equal amounts
        return 1
    else:
        # Otherwise it's the most_common number
        return int(most[0][0])


def get_co2_common_bit(column):
    # For the co2 we want the least common number or 0 if equal amounts
    least = Counter(column).most_common(2)
    if len(least) > 1 and least[0][1] == least[1][1]:
        # If there are equal amounts
        return 0
    elif len(least) > 1:
        # If there are more than one option we want the second one
        return int(least[1][0])
    else:
        # If there's only one option we return that one
        return int(least[0][0])


def remove_from_list_with_bit(bit, inputs, column_num):
    remove_bit = not bool(bit)
    # Have to copy the list otherwise the for loop cuts out early
    mylist = inputs[:]
    for line in mylist:
        if int(get_nth_char(column_num, line)) == int(remove_bit):
            inputs.remove(line)
    return inputs


def get_nth_column(inputs, column_num):
    column_chars = []
    for line in inputs:
        column_chars.append(get_nth_char(column_num, line))
    return column_chars


def get_nth_char(n, string):
    return list(string)[n]


file = open('inputs.txt', 'r')
file = file.read().splitlines()
num_col = len(file[0])
# Have to do deep copies otherwise the lists are the same object
oxy_output = copy.deepcopy(file)
co2_output = copy.deepcopy(file)

for column in range(num_col):
    if len(oxy_output) > 1:
        curr_column = get_nth_column(oxy_output, column)
        oxy_output = remove_from_list_with_bit(
            get_oxy_common_bit(curr_column), oxy_output, column)

    if len(co2_output) > 1:
        curr_column = get_nth_column(co2_output, column)
        co2_output = remove_from_list_with_bit(
            get_co2_common_bit(curr_column), co2_output, column)


# Cast to int as a binary number!
oxy_output = int(oxy_output[0], 2)
co2_output = int(co2_output[0], 2)
print(oxy_output * co2_output)
