file = open('dec_6/input.txt', 'r')
initial_fish = file.readline().split(',')
fish_list = list(map(int, initial_fish))

curr_days = 0
end_days = 80

for day in range(1, end_days+1):
    for i, fish in enumerate(fish_list):
        if fish_list[i] == 0:
            fish_list[i] = 6
            # Add a 9 because it will be decreased today
            fish_list.append(9)
        else:
            fish_list[i] -= 1

print(len(fish_list))
