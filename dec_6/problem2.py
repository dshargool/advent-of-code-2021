from collections import Counter, defaultdict

file = open('dec_6/input.txt', 'r')
initial_fish = file.readline().split(',')
fish_list = list(map(int, initial_fish))

curr_days = 0
end_days = 256
f = Counter(fish_list)

for day in range(end_days):
    a = defaultdict(int)
    for bin in f:
        if bin > 0:
            # Need to use += so that we add the 6 and 8 fishes when those are computed if they're computed first
            a[bin - 1] += f[bin]
        else:
            a[6] += f[bin]
            a[8] += f[bin]

    f = a

print(sum(f.values()))
