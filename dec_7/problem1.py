from collections import Counter
import statistics

file = open('./dec_7/input.txt','r')
crab_positions = file.read().split(',')
crab_positions = list(map(int, crab_positions))
optimal_position = round(statistics.median(crab_positions))
print(optimal_position)

crab_positions = Counter(crab_positions)
print(crab_positions)
fuel = 0
for position in crab_positions:
    fuel += abs(position - optimal_position) * crab_positions[position]
print(fuel)