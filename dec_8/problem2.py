def draw_line(x1, y1, x2, y2, map):
    lx = abs(x2 - x1)
    ly = abs(y2 - y1)
    if x2 > x1:
        sx = 1
    elif x2 == x1:
        sx = 0
    else:
        sx = -1
    if y2 > y1:
        sy = 1
    elif y2 == y1:
        sy = 0
    else:
        sy = -1

    for i in range(max(lx, ly)+1):
        x = x1 + sx * i
        y = y1 + sy * i
        map[x][y] += 1


def calc_danger_zones(map):
    score = 0
    for rows in map:
        for cols in rows:
            if cols > 1:
                score += 1
    return score


map_size = 1000
map = [[0]*map_size for i in range(map_size)]
file = open('input.txt', 'r')
for line in file:
    print(line)
    coords = line.split()
    orig = coords[0].split(',')
    end = coords[2].split(',')
    x_1 = int(orig[0])
    y_1 = int(orig[1])
    x_2 = int(end[0])
    y_2 = int(end[1])

    draw_line(x_1, y_1, x_2, y_2, map)

print(calc_danger_zones(map))