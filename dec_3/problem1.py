from collections import Counter


def get_nth_column(inputs, column):
    column_chars = []
    for line in inputs:
        column_chars.append(get_nth_char(column, line))
    return column_chars


def get_most_common_char(column):
    return Counter(column).most_common(1)[0][0]


def get_nth_char(n, string):
    return list(string)[n]


file = open('inputs.txt', 'r')
file = file.read().splitlines()
num_col = len(file[0])
output = 0
for column in range(num_col):
    print(column)
    input_col = get_nth_column(file, column)
    bit_out = int(get_most_common_char(input_col)) << num_col - 1 - column
    print(bin(bit_out))
    output = output | bit_out
most_common = output
least_common = ~ most_common & 0xFFF

print(most_common * least_common)

print(bin(most_common))
print(bin(least_common))
