file = open('inputs.txt', 'r')
horizontal = 0
depth = 0
for lines in file:
    command = lines.split()
    direction = command[0]
    amount = int(command[1])
    if direction == "forward":
        horizontal = horizontal + amount
    elif direction == "up":
        depth = depth - amount
    elif direction == "down":
        depth = depth + amount
print(horizontal * depth)
